<?php

require_once(__DIR__ . '/vendor/autoload.php');


function interrogeAPIinscription($url, $form_params = [])
{
    return simplasso_prepare_client_api('inscription/' . $url, $form_params);
}


function interrogeAPIactivation($url, $form_params = [])
{
    return simplasso_prepare_client_api('activation/' . $url, $form_params);
}

function interrogeAPIdata($url, $form_params = [])
{
    return simplasso_prepare_client_api('data/' . $url, $form_params);
}


function simplasso_prepare_client_api($url, $form_params, $headers = [])
{
    include_spip('inc/config');
    $client = new GuzzleHttp\Client();
    $erreurs=[];
    $response = '';
    $headers = [
            'Accept' => 'application/json',
        ] + $headers;
    $methode = 'GET';
    $args = ['headers' => $headers];
    $verify = lire_config('simplasso/verifier_certificat','on');
    $args['verify'] = ($verify==='on');

    if (!empty($form_params)) {
        $methode = 'POST';
        $args['form_params'] = $form_params;
    }
    $url_logiciel = lire_config('simplasso/url_logiciel');
    $url = $url_logiciel . 'api/' . $url;
    try {
        $response = $client->request($methode, $url, $args);
        return json_decode($response->getBody(), true);
    } catch (\Exception $ex) {
        $erreurs[] = simplasso_gestion_erreur($ex, $response);
        print_r($ex->getMessage());
        $ok = false;
    }
    return $ok;

}


function interrogeAPITransaction($url, $form_params = [])
{
    return simplasso_prepare_client_api($url, $form_params);
}


function simplasso_gestion_erreur(\Exception $ex, $response)
{

    $erreurs = [];

    if (isset($response)) {


        $erreurs[] = 'Probleme dans la connexion';
        if (!is_array($response) && method_exists($response, 'getBody')) {
            $response = $ex->getResponse();
            $responseBodyAsString = $response->getBody()->getContents();
            print_r($responseBodyAsString);
            exit();
        } else {
            $response = $ex->getResponse();

            if($response){
                if ($response->getStatusCode() === 401 ) {
                    include_spip('inc/headers');
                    redirige_par_entete(generer_url_action('logout')."&logout=public&url=".urlencode(generer_url_public("espace_adherent","eject=1")));
                } else {
                    $responseBodyAsString = $response->getBody()->getContents();
                    //print_r($responseBodyAsString);
                }
            }

            //session_unset('simplasso_user_token');
        }
    } else {
        $response = $ex->getResponse();
        $responseBodyAsString = $response->getBody()->getContents();
        //print_r($responseBodyAsString);
    }
    return $erreurs;
}


function interrogeAPI($url, $form_params = [])
{
    $token = session_get('simplasso_user_token');
    $client = new GuzzleHttp\Client();
    $response = [];
    if (!empty($token)) {
        try {
            $headers = [
                'Authorization' => 'Bearer ' . $token,
                'Accept' => 'application/json',
            ];
            $methode = 'GET';
            $args = ['headers' => $headers];
            if (!empty($form_params)) {
                $methode = 'POST';
                $args['form_params'] = $form_params;
            }
            $verify = lire_config('simplasso/verifier_certificat','on');
            $args['verify'] = ($verify==='on');
            $url_logiciel = lire_config('simplasso/url_logiciel');
            $url = $url_logiciel . 'api/' . $url;
            $response = $client->request($methode, $url, $args);
            $response = json_decode($response->getBody(), true);

        } catch (\Exception $ex) {
            $erreurs[] = simplasso_gestion_erreur($ex, $response);
            $ok = false;
        }
    }
    return $response;
}



function interrogeAPIDocument($url, $form_params = [])
{
    include_spip('inc/session');
    $token = session_get('simplasso_user_token');

    $ok=false;
    $response=[];
    if (!empty($token)) {
        try {

            $headers = [
                'Authorization' => 'Bearer ' . $token
            ];

            $methode = 'GET';
            $args = [
                'headers' => $headers,
                'stream' => true
                ];
            include_spip('inc/config');
            $url_logiciel = lire_config('simplasso/url_logiciel');
            $url = $url_logiciel . 'api/' . $url;
            $verify = lire_config('simplasso/verifier_certificat','on');
            $args['verify'] = ($verify==='on');

            if (!empty($form_params)) {
                $methode = 'POST';
                $args['form_params'] = $form_params;
            }
            $client = new GuzzleHttp\Client();
            $response = $client->request($methode, $url, $args);
            header("Content-type:application/x-pdf");
            header("Content-Disposition:attachment;filename=recu_fiscal.pdf");
            $body = $response->getBody();
            while (!$body->eof()) {
                echo $body->read(1024);
            }
            $ok = true;

        } catch (\Exception $ex) {
            //var_dump($ex);
            $erreurs[] = simplasso_gestion_erreur($ex, $response);
        }
    }
    return $ok;
}


function balise_INFOS_ADHERENT_dist($p)
{
    $p->code = 'interrogeAPI(\'info_adherent\')';
    return $p;
}


function balise_LISTE_COTISATION_dist($p)
{
    $p->code = 'interrogeAPI(\'info_cotisation\')';
    return $p;
}


function balise_LISTE_DON_dist($p)
{
    $p->code = 'interrogeAPI(\'info_don\')';
    return $p;
}

function balise_LISTE_TRANSACTION_dist($p)
{
    $p->code = 'interrogeAPI(\'info_transaction_en_attente\')';
    return $p;
}


function balise_SIMPLASSO_TEST_CONNECTION_dist($p)
{
    $p->code = 'simplasso_test_connection()';
    return $p;
}


function balise_LISTE_PAYS_dist($p)
{
    $p->code = 'liste_pays(true)';
    return $p;
}


function balise_LISTE_ENTITE_dist($p)
{

    $p->code = 'liste_entite()';
    return $p;
}

include_spip('public/cacher');

function liste_entite($force_refresh = false)
{
    $tab = simplasso_lire_cache('liste_entite');
    if (empty($tab) || $force_refresh) {
        $tab = interrogeAPIdata('entites');
        simplasso_ecrire_cache('liste_entite', $tab);
    }
    return $tab;
}


function transfertRecuFiscal($args)
{
    return interrogeAPIDocument('recu_fiscal',['id_document'=>$args]);
}

/**
 * ecrire le cache dans un casier
 *
 * @param string $nom_cache
 * @param $valeur
 * @return bool
 */
function simplasso_ecrire_cache($nom_cache, $valeur)
{
    $d = substr($nom_cache, 0, 2);
    $u = substr($nom_cache, 2, 2);
    $rep = _DIR_CACHE;
    $rep = sous_repertoire($rep, '', false, true);
    $rep = sous_repertoire($rep, $d, false, true);

    return ecrire_fichier($rep . $u . ".cache", serialize(array("nom_cache" => $nom_cache, "valeur" => $valeur)));
}

/**
 * lire le cache depuis un casier
 *
 * @param string $nom_cache
 * @return mixed
 */
function simplasso_lire_cache($nom_cache)
{
    $d = substr($nom_cache, 0, 2);
    $u = substr($nom_cache, 2, 2);

    if (file_exists($f = _DIR_CACHE . "$d/$u.cache")
        and lire_fichier($f, $tmp)
        and $tmp = unserialize($tmp)
        and $tmp['nom_cache'] == $nom_cache
        and isset($tmp['valeur'])
    ) {
        return $tmp['valeur'];
    }

    return false;
}





function liste_pays($force_refresh = false)
{
    $tab = simplasso_lire_cache('liste_pays');
    if (empty($tab) || $force_refresh) {
        include_spip('inc/jsonrpc');
        $tab = interrogeAPIdata('pays');
        simplasso_ecrire_cache('liste_pays', $tab);
    }
    return $tab;
}


function simplasso_liste_prestation($force_refresh = false)
{
    $tab = simplasso_lire_cache('liste_prestation');
    if (empty($tab) || $force_refresh) {
        include_spip('inc/jsonrpc');
        $tab = interrogeAPIinscription('liste_prestation');
        simplasso_ecrire_cache('liste_prestation', $tab);
    }
    return $tab;
}

function balise_NB_ENTITE_dist($p)
{
    $p->code = 'nb_entite()';
    return $p;
}

function json_valeur($texte, $var)
{
    $tab = json_decode($texte, true);
    if (isset($tab[$var])) {
        return $tab[$var];
    }
    return '';
}


function simplasso_test_connection(){
    return (count(liste_pays(true))>0)?'OK':'EN ECHEC';
}


function nb_entite()
{
    return count(liste_entite());
}


function simplasso_url_rubrique($tab_o){
    $url = '';
    if(isset($tab_o[0])){
        $tab = explode('|',$tab_o[0]);
        if($tab[1]>0){
            return generer_url_public('rubrique',['id_rubrique'=>$tab[1]]);
        }
    }
    return $url;
}


function getSimplassoPays($code){

    $tab_pays = liste_pays();
    if(isset( $tab_pays[$code])){
        return $tab_pays[$code];
    }
    return $code;
}


function balise_SIMPLASSO_GROUPE_INDIVIDUS_dist($p){
    return calculer_balise_dynamique ( $p,'SIMPLASSO_GROUPE_INDIVIDUS',[]);
}

function balise_SIMPLASSO_GROUPE_INDIVIDUS_stat($args,$context) {
    return [['groupe'=>$args[0],'template_cplt'=>$args[1]??'','info_cplt'=>$args[2]??'0','recursif'=>$args[3]??'0'],$context];
}

function balise_SIMPLASSO_GROUPE_INDIVIDUS_dyn($args) {
    $tab = interrogeAPI('groupeindividus/?groupe='.$args['groupe'].'&info_cplt='.$args['info_cplt'].'&recursif='.$args['recursif']);
    $tab['groupe']=$args['groupe'];
    return ['inclure/adh_groupe_individus'.$args['template_cplt'], 3600, $tab];
}



function balise_SIMPLASSO_GROUPE_INDIVIDUS_ID_dist($p){
    if ($p->param and ($c = $p->param[0])) {
        // liste d'arguments commence toujours par la chaine vide
        array_shift($c);
        // construire la liste d'arguments comme pour un filtre
        $param = compose_filtres_args($p, $c, '');
    } else {
        $param = '';
    };
    $p->code = 'array_keys((interrogeAPI(\'groupeindividus/?groupe='.trim($param,'\'').'\')[\'tab_groupe_individus\']))';
    return $p;
}



function balise_SIMPLASSO_GROUPE_INDIVIDUS_RESULTAT_dist($p){
    if ($p->param and ($c = $p->param[0])) {
        // liste d'arguments commence toujours par la chaine vide
        array_shift($c);
        // construire la liste d'arguments comme pour un filtre
        $param = compose_filtres_args($p, $c, '');
    } else {
        $param = '';
    };
    $p->code = '(interrogeAPI(\'groupeindividus/?groupe='.trim($param,'\'').'\')[\'tab_groupe_individus\'])';
    return $p;
}
