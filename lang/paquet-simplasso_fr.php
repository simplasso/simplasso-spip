<?php
if (!defined('_ECRIRE_INC_VERSION')) return;
$GLOBALS[$GLOBALS['idx_lang']] = array(
	
	'simplasso_description' => 'Faire communiquer Spip et le logiciel Simplasso. Simplasso est un logiciel de gestion associative.',
	'simplasso_nom' => 'Simplasso',
	'simplasso_slogan' => 'Branchez vous à Simplasso',
	
);
?>
