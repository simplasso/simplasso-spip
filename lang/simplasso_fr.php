<?php
/***************************************************************************
 *  Associaspip, extension de SPIP pour gestion d'associations             *
 *                                                                         *
 *  Copyright (c) 2007 Bernard Blazin & Francois de Montlivault (V1)       *
 *  Copyright (c) 2010-2011 Emmanuel Saint-James & Jeannot Lapin (V2)       *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined("_ECRIRE_INC_VERSION")) return;

// This is a SPIP language file  --  Ceci est un fichier langue de SPIP

$GLOBALS[$GLOBALS['idx_lang']] = array(

	'tableau_de_bord' => 'Tableau de bord',
	'titre_page_config' => 'Configuration du plugin',
	'erreur_probleme_technique' => 'Problème technique',
	'erreur_probleme_technique2' => 'Problème technique',
	'ok_mdp_modifie' => 'Votre mot de passe a bien été modifié',
	'configuration_generale' => 'Configuration générale',
	'url_logiciel' => 'URL de l\'API simplasso',
	'acces_restreints' => 'id_auteur_adherent ',
	'compte_generique' => 'Compte générique',
	'id_auteur_adherent' => 'Auteur adhérent',
	'acces_donnees_spip' => 'Accès aux données de SPIP',
	'acces_donnees_spip_ouinon' => 'Autoriser l\'accès aux données de SPIP',
	'mailsubscribers_ouinon' => 'Autoriser  l\'accès aux données de mailsubscribers',
	'acces_restreints_ouinon' => 'Autoriser l\'accès aux données de mailsubscribers',
	'restriction_ip' => 'Adresse IP acceptées',
	'adresse' => 'Adresse',
	'ville' => 'Ville',
	'code_postal' => 'Code postal',
	'pays' => 'Pays',
	'mobile' => 'Mobile',
	'telephone' => 'Téléphone',
	'email' => 'Email',
	'simplasso' => 'Simplasso',
	'recevoir_mail_activation' => 'Un courriel vient de vous être envoyé, il contient les informations pour poursuivre la procédure.',
	'ok_connection_possible'=>'Vous pouvez désormais accéder à votre compte adhérent en utilisant votre login et le mot de passe que vous venez de saisir.',
	'form_erreur_accepter_condition'=>'Vous devez accepter les CGU pour finaliser votre inscription',
	'erreur_password_obligatoire'=>'La saisie d\'un mot de passe est obligatoire' ,
	'erreur_email_deja_enregistre'=>'Votre courriel @email@ existe déjà. Passez par votre espace adhérent pour effectuer votre don.' ,
	'erreur_email_deja_enregistre_don'=>'Votre courriel @email@ existe déjà. Passez par votre espace adhérent pour effectuer votre don.' ,
	'erreur_password_trop_court'=>'Le mot de passe doit comporter au minium 8 caractères' ,
	'erreur_password_non_identique'=>'Les mots de passe saisis ne sont pas identiques' ,


	'label_adresse'=>'Adresse',
	'label_adresse_cplt'=>'Adresse complémentaire',
	'label_annee_de_naissance'=>'Année de naissance',
	'label_civilite'=>'Civilité',
	'label_coordonnees'=>'Coordonnées',
	'label_code_postal'=>'Code postal',

	'label_email_sos'=>'Email support',
	'label_ville'=>'Ville',
	'label_bio'=>'Commentaire',
	'label_login'=>'Nom d\'utilisateur(login)',
	'label_nom'=>'Nom',
	'label_organisme'=>'Organisme',
	'label_nom_site'=>'Nom de votre site',
	'label_prenom'=>'Prénom',
	'label_url_site'=>'Url de votre site',
	'label_identite'=>'Identité',
	'label_moyen_de_communication'=>'Moyen de communication',
	'label_newsletter' => 'Inscrit aux newsletters',
	'label_personne'=>'Personne',
	'label_telephone_fixe'=>'Téléphone fixe',
	'label_fax'=>'Fax',
	'label_divers'=>'Divers',
	'label_sexe'=>'Sexe',
	'label_date_de_naissance'=>'Date de naissance',
	'label_commentaire'=>'Commentaire',
	'label_contact_souhait'=>'Accepte d\'être contacter',
	'label_profession'=>'Profession',
	'label_email'=>'Courriel',
	'label_courriel'=>'Courriel',
	'label_telephone_mobile'=>'Téléphone mobile',
	'label_cotisation'=>'Cotisation',
	'label_don'=>'Don',
	'label_montant_don'=>'Montant',
	'label_montant_autre'=>'Autre',
	'label_mot_de_passe'=>'Mot de passe',
	'label_mot_de_passe_confirmation'=>'Confirmation mot de passe',
	'bouton_etape_suivante'=>'Étape suivante',
	'label_accepter_condition'=>'Accepter les conditions générales d\'utilisation',
	'label_courriel_login'=>'Courriel',
	'info_cplt_obligatoire'=>'Informations obligatoires',
	'Informations_diverses'=>'Informations diverses',

	'label_autre_montant'=> 'Autre montant',
	'montant_du_don'=> 'Montant du don',
	'don_question'=> 'Voulez vous faire un don ?',
	'label_type_cotisation'=> 'Type de cotisation',
	'bouton_se_connecter' => 'Se connecter'


);




