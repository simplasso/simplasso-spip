<?php
/**
 * Plugin OpenID
 * Licence GPL (c) 2007-2009 Edouard Lafargue, Mathieu Marcillaud, Cedric Morin, Fil
 *
 */
if (!defined("_ECRIRE_INC_VERSION")) return;

include_spip('inc/meta');

/**
 * Upgrade de la base
 *
 * @param string $nom_meta_base_version
 * @param string $version_cible
 */
function simplasso_upgrade($nom_meta_base_version, $version_cible)
{
    $current_version = 0.0;

    if ((!isset($GLOBALS['meta'][$nom_meta_base_version]))
        || (($current_version = $GLOBALS['meta'][$nom_meta_base_version]) != $version_cible)
    ) {
        include_spip('base/simplasso');
        if ($current_version == 0.0) {
            include_spip('base/create');
            maj_tables('spip_auteurs');
            ecrire_meta($nom_meta_base_version, $current_version = $version_cible, 'non');
        }

    }
}

/**
 * Desinstallation du plugin
 *
 * @param string $nom_meta_base_version
 */
function simplasso_vider_tables($nom_meta_base_version)
{
    sql_alter("TABLE spip_auteurs DROP simplasso");
    effacer_meta($nom_meta_base_version);
}


