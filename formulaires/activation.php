<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) return;

// chargement des valeurs par defaut des champs du formulaire
function formulaires_activation_charger_dist($mdp_oublie=false){

	$valeurs = array('mdp_oublie'=>$mdp_oublie);
	return $valeurs;
}


// fonction qu'on peut redefinir pour filtrer les adresses mail
// http://doc.spip.org/@test_activation
function test_activation_dist($email)
{
	include_spip('inc/filtres'); # pour email_valide()
	if (!email_valide($email) )
		return _T('pass_erreur_non_valide', array('email_activation' => htmlspecialchars($email)));
	return array('mail' => $email);
}



function formulaires_activation_verifier_dist($mdp_oublie=false)
{
	$erreurs = array();
	$email = _request('activation');

	$reponse = interrogeAPIactivation( 'test',['email'=>$email] );
	if (isset($reponse['ok'])) {
		if (!$reponse['ok']) {
			$erreurs['activation'] = _T('pass_erreur_non_enregistre', array('email_oubli' => htmlspecialchars($email)));
		}
	}
	else {
		$erreurs['activation'] = _T('pass_erreur_acces_refuse');
	}
	return $erreurs;
}



// la saisie a ete validee, on peut agir
function formulaires_activation_traiter_dist($mdp_oublie=false){

	$email = _request('activation');
	include_spip('inc/texte'); # pour corriger_typo

	$args = array(
		'email'=> trim($email),
		'nom_site'=> textebrut(corriger_typo($GLOBALS['meta']["nom_site"])),
		'url_activation'=> url_absolue(generer_url_public('activation',array('email'=>$email))),
		'activation'=>($mdp_oublie==false)
	);

	$reponse = interrogeAPIactivation('',$args);

	if (isset($reponse['ok']) ){
		if ($reponse['ok'] )
			$message= _T('simplasso:recevoir_mail_activation');
		else
			$message=  _T('simplasso:erreur_probleme_technique');
	}else{
		$message=  _T('simplasso:erreur_probleme_technique');
    }
	return array('message_ok'=>$message,'editable'=>'','mdp_oublie'=>$mdp_oublie);
}





