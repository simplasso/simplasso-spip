<?php


if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');



// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_rgpd_adherent_charger_dist() {
    $valeurs=[];
    $valeurs['_mes_saisies'] = simplasso_saisie_adherent();
    $tab_data = interrogeAPI('info_adherent');
    if(isset($tab_data['rgpd'])){
        foreach($tab_data['rgpd'] as $r)
            $valeurs['question'.$r['id']] = $r['reponse'];
    }
    return $valeurs;
}





function formulaires_rgpd_adherent_verifier_dist() {

    $mes_saisies = simplasso_saisie_adherent();
    $erreurs = saisies_verifier($mes_saisies);
    return $erreurs;
}


function formulaires_rgpd_adherent_traiter_dist()
{
    $tab_champs = array_keys(simplasso_saisie_adherent());
    $args = [];
    $ok=false;
    $tab_data = interrogeAPI('info_adherent');
    $args['reponses'] = [];
    if (isset($tab_data['rgpd'])) {
        foreach ($tab_data['rgpd'] as $rgpd) {
            $args['reponses'][$rgpd['id']] = _request('question' . $rgpd['id']);
        }

    include_spip('inc/jsonrpc');
    $ok = interrogeAPI('adherent_rgpd', $args);

    }
    if ($ok) {

        $tab = array('message_ok' => 'Vos modifications ont bien été enregistrées.');
        $tab['redirect'] = generer_url_public('espace_adherent');
        return $tab;
    } else {
        return array('message_erreur' => 'Error');
    }
}


function simplasso_saisie_adherent() {

    include_spip('inc/config');
    $tab_rgpd=[];
    $tab_champs=[];
    $tab_data = interrogeAPI('info_adherent');
    if(isset($tab_data['rgpd'])){
        $tab_rgpd = $tab_data['rgpd'];
    }

    foreach($tab_rgpd as $rgpd){
        $id = $rgpd['id'];
        $tab_champs['question'.$id] =
            [
                'saisie' => 'oui_non',
                'options' => [
                    'nom' => 'question'.$id,
                    'label' => $rgpd['question'],
                    'valeur_oui' => 'oui',
                    'valeur_non' => 'non'
                ]
            ];
    }
    return $tab_champs;
}
