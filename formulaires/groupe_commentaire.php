<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
 * \***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');


// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_groupe_commentaire_charger_dist($groupe,$commentaire) {
    $valeurs=[
        'groupe_nomcourt'=>$groupe,
        'commentaire'=>$commentaire
    ];
    $valeurs['_mes_saisies'] = simplasso_groupe_commentaire();
    return $valeurs;
}





function formulaires_groupe_commentaire_verifier_dist($groupe,$commentaire): array
{
    $mes_saisies = simplasso_groupe_commentaire($groupe);
    $erreurs = saisies_verifier($mes_saisies);
    return $erreurs;
}


function formulaires_groupe_commentaire_traiter_dist($groupe,$commentaire): array
{

    $groupe_nomcourt = _request('groupe_nomcourt');
    $args=['groupe_nomcourt'=>$groupe_nomcourt,'commentaire'=> _request('commentaire')];

    include_spip('inc/jsonrpc');
    $reponse = interrogeAPI('groupe_commentaire', $args);
    if ($reponse['ok']) {
        $url_redirect = _request('url_redirect',generer_url_public('espace_adherent'));
        $tab=[
            'redirect' => $url_redirect,
            'message_ok' => 'Votre commentaire a bien été enregistré .'
        ];
        return $tab;

    } else {
        return array('message_erreur' => 'Erreur');
    }
}


function simplasso_groupe_commentaire(): array
{

    include_spip('inc/config');


    $tab_champs = [

        // Champ adresse
        'groupe_nomcourt' => array(
            'saisie' => 'hidden',
            'options' => array(
                'nom' => 'groupe_nomcourt',
                'obligatoire' => 'oui',
            )
        ),

        // Champ adresse
        'commentaire' => array(
            'saisie' => 'textarea',
            'options' => array(
                'nom' => 'commentaire',
                'label' => _T('commentaires'),
                'class' => '',
                'obligatoire' => 'non',
                'rows'=>3
            )
        )
    ];

    return $tab_champs;
}
