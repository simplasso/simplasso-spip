<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
 * \***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');

function instituer_auteur_ici($auteur = array()) {
    $instituer_auteur = charger_fonction('instituer_auteur', 'inc');
    return $instituer_auteur($auteur);
}

// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_info_adherent_charger_dist() {
    $valeurs=[];
    $valeurs['_mes_saisies'] = simplasso_saisie_adherent();
    $tab_data = interrogeAPI('info_adherent');
    $valeurs = array_merge($valeurs, $tab_data);
    return $valeurs;
}





function formulaires_info_adherent_verifier_dist() {

    $mes_saisies = simplasso_saisie_adherent();
    $erreurs = saisies_verifier($mes_saisies);
    include_spip('inc/jsonrpc');
    $ok = interrogeAPI('adherent_test_email', array(_request('email')));
    if (!$ok) {
        $erreurs['email'] = _T('form_email_non_valide');
    }
    return $erreurs;
}


function formulaires_info_adherent_traiter_dist() {
    $tab_champs = array_keys(simplasso_saisie_adherent());
    $args=[];
    foreach ($tab_champs as $champs) {
        $args[$champs] = _request($champs);
    }

    include_spip('inc/jsonrpc');
    $ok = interrogeAPI('adherent_modification', $args);
    if ($ok) {
        $tab=[
            'redirect' => generer_url_public('espace_adherent'),
            'message_ok' => 'Vos modifications ont bien été enregistrées.'
        ];
        return $tab;
        
    } else {
        return array('message_erreur' => 'Error');
    }
}


function simplasso_saisie_adherent() {

    include_spip('inc/config');
    $code_france = lire_config('simplasso/pays_defaut');

    $tab_champs = array(

        // Champ adresse
        'adresse' => array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'adresse',
                'label' => _T('simplasso:label_adresse'),
                'obligatoire' => 'oui',
            )
        ),

        // Champ adresse
        'adresseCplt' => array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'adresseCplt',
                'label' => _T('simplasso:label_adresse_cplt'),
                'obligatoire' => 'non'
            )
        ),

        // Champ code postal
        'codepostal' => array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'codepostal',
                'label' => _T('simplasso:code_postal'),
                'obligatoire' => 'oui',
                'attributs'=> 'data-ville="champ_ville"',
            )
        ),
        // Champ  ville
        'ville' => array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'ville',
                'label' => _T('simplasso:ville'),
                'obligatoire' => 'oui'
            )
        ),
        //Champ Pays
        'pays' => array(
            'saisie' => 'selection',
            'options' => array(
                'nom' => 'pays',
                'label' => _T('simplasso:pays'),
                'class' => '',
                'datas' => liste_pays(),
                'defaut' => $code_france,
                'obligatoire' => 'oui',

            )
        ),
        // Champ telephone
        'telephone' => array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'telephone',
                'label' => _T('simplasso:telephone'),
                'obligatoire' => 'non',
                'class' => 'masque_telephone'
            )
        ),
        // Champ telephone mobile
        'mobile' => array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'mobile',
                'label' => _T('simplasso:mobile'),
                'class' => 'masque_telephone',
                'obligatoire' => 'non'
            )
        ),
        // Champ courriel
        'email' => array(
            'saisie' => 'input',
            'options' => array(
                'nom' => 'email',
                'label' => _T('simplasso:email'),
                'obligatoire' => 'non',
                'class' => 'multiple_par_separateur'
            )
        )
    );

    return $tab_champs;
}
