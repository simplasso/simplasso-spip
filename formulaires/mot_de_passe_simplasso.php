<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2016                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
\***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('base/abstract_sql');


// chargement des valeurs par defaut des champs du formulaire
/**
 * Chargement de l'auteur qui peut changer son mot de passe.
 * Soit un cookie d'newpass fourni par #FORMULAIRE_newpass est passe dans l'url par &p=
 * Soit un id_auteur est passe en parametre #FORMULAIRE_MOT_DE_PASSE{#ID_AUTEUR}
 * Dans les deux cas on verifie que l'auteur est autorise
 *
 * @param int $id_auteur
 * @return array
 */
function formulaires_mot_de_passe_simplasso_charger_dist() {

	$valeurs = array();
	$valeurs['editable'] = true;
	$valeurs['token'] = _request('token');
	$valeurs['email'] = _request('email');
	$valeurs['newpass'] = '';
	$valeurs['nobot'] = '';

	return $valeurs;
}

/**
 * Verification de la saisie du mot de passe.
 * On verifie qu'un mot de passe est saisi, et que sa longuer est suffisante
 * Ce serait le lieu pour verifier sa qualite (caracteres speciaux ...)
 *
 * @param int $id_auteur
 */
function formulaires_mot_de_passe_simplasso_verifier_dist() {
	$erreurs = array();
	if (!_request('newpass')) {
		$erreurs['newpass'] = _T('info_obligatoire');
	} else {
		if (strlen($p = _request('newpass')) < _PASS_LONGUEUR_MINI) {
			$erreurs['newpass'] = _T('info_passe_trop_court_car_pluriel', array('nb' => _PASS_LONGUEUR_MINI));
		} else {
			if (!is_null($c = _request('newpass_confirm'))) {
				if (!$c) {
					$erreurs['newpass_confirm'] = _T('info_obligatoire');
				} elseif ($c !== $p) {
					$erreurs['newpass'] = _T('info_passes_identiques');
				}
			}
		}
	}
	if (isset($erreurs['newpass'])) {
		set_request('newpass');
		set_request('newpass_confirm');
	}

	if (_request('nobot')) {
		$erreurs['message_erreur'] = _T('pass_rien_a_faire_ici');
	}

	return $erreurs;
}

/**
 * Modification du mot de passe d'un auteur.
 * Utilise le cookie d'newpass fourni en url ou l'argument du formulaire pour identifier l'auteur
 *
 * @param int $id_auteur
 */
function formulaires_mot_de_passe_simplasso_traiter_dist() {
	
	include_spip('inc/jsonrpc');
	$mot_de_passe = _request('newpass');
	
	$args = array(
		'password'=> $mot_de_passe
	);
	$ok = interrogeAPI('mot_de_passe',$args);

	if ($ok)
		$message= _T('simplasso:ok_mdp_modifie');
	else
		$message=  _T('simplasso:erreur_probleme_technique');


	return [
	    'message_ok'=>$message,
        'redirect' => generer_url_public('espace_adherent')
        ];
}
