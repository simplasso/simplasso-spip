<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2016                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
 * \***************************************************************************/

/**
 * Gestion du formulaire d'identification / de connexion à SPIP
 *
 * @package SPIP\Core\Formulaires
 **/


if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('base/abstract_sql');


/**
 * Chargement du formulaire de login
 *
 * Si on est déjà connecté, on redirige directement sur l'URL cible !
 *
 * @uses auth_informer_login()
 * @uses is_url_prive()
 * @uses login_simplasso_auth_http()
 *
 * @param string $cible
 *     URL de destination après identification.
 *     Cas spécifique : la valeur `@page_auteur` permet d'être redirigé
 *     après connexion sur le squelette public de l'auteur qui se connecte.
 * @param string $login
 *     Login de la personne à identifier (si connu)
 * @param null|bool $prive
 *     Identifier pour l'espace privé (true), public (false)
 *     ou automatiquement (null) en fonction de la destination de l'URL cible.
 * @return array
 *     Environnement du formulaire
 *     
 **/
function formulaires_login_simplasso_charger_dist($cible = "", $login = "", $prive = null, $autofocus=false)
{
    $erreur = _request('var_erreur');

    if (!$login) {
        $login = strval(_request('var_login'));
    }
    // si on est deja identifie
    if (!$login and isset($GLOBALS['visiteur_session']['login'])) {
        $login = $GLOBALS['visiteur_session']['login'];
    }


    include_spip('inc/auth');
    $row = auth_informer_login($login);

    // Construire l'environnement du squelette
    // Ne pas proposer de "rester connecte quelques jours"
    // si la duree de l'alea est inferieure a 12 h (valeur par defaut)

    $valeurs = array(
        'var_login' => $login,
        'editable' => !$row,
        'cnx' => isset($row['cnx']) ? $row['cnx'] : '',
    );

    if ($erreur or !isset($GLOBALS['visiteur_session']['id_auteur']) or !$GLOBALS['visiteur_session']['id_auteur']) {
        $valeurs['editable'] = true;
    }


    // Si on est connecte, appeler traiter()
    // et lancer la redirection si besoin
    if (!$valeurs['editable'] ) {
        $traiter = charger_fonction('traiter', 'formulaires/login');
        $res = $traiter($cible, $login, $prive);
        $valeurs = array_merge($valeurs, $res);

        if (isset($res['redirect']) and $res['redirect']) {
            include_spip('inc/headers');
            # preparer un lien pour quand redirige_formulaire ne fonctionne pas
            $m = redirige_formulaire($res['redirect']);
            $valeurs['_deja_loge'] = inserer_attribut(
                "<a>" . _T('login_simplasso_par_ici') . "</a>$m",
                'href', $res['redirect']
            );
        }
    }
    // en cas d'echec de cookie, inc_auth a renvoye vers le script de
    // pose de cookie ; s'il n'est pas la, c'est echec cookie
    // s'il est la, c'est probablement un bookmark sur bonjour=oui,
    // et pas un echec cookie.
    if ($erreur == 'cookie') {
        $valeurs['echec_cookie'] = ' ';
    } elseif ($erreur) {
        // une erreur d'un SSO indique dans la redirection vers ici
        // mais il faut se proteger de toute tentative d'injection malveilante
        include_spip('inc/texte');
        $valeurs['message_erreur'] = safehtml($erreur);
    }

    $valeurs['autofocus'] = $autofocus;

    return $valeurs;
}


/**
 * Vérifications du formulaire de login
 *
 * Connecte la personne si l'identification réussie.
 *
 * @uses auth_identifier_login()
 * @uses auth_loger()
 * @uses login_simplasso_autoriser()
 *
 * @param string $cible
 *     URL de destination après identification.
 *     Cas spécifique : la valeur `@page_auteur` permet d'être redirigé
 *     après connexion sur le squelette public de l'auteur qui se connecte.
 * @param string $login
 *     Login de la personne à identifier (si connu)
 * @param null|bool $prive
 *     Identifier pour l'espace privé (true), public (false)
 *     ou automatiquement (null) en fonction de la destination de l'URL cible.
 * @return array
 *     Erreurs du formulaire
 **/
function formulaires_login_simplasso_verifier_dist($cible = "", $login = "", $prive = null)
{

    $session_login = _request('var_login');
    $session_password = _request('password');
    $erreurs=[];

    if (!$session_login) {
        return array('var_login' => _T('info_obligatoire'));
    }


    $ok=false;

	require_once(__DIR__ . '/../vendor/autoload.php');
	$client  = new GuzzleHttp\Client();

	try {
        $url_logiciel = lire_config('simplasso/url_logiciel');

        $args = ['json' => ['username' => $session_login,'password'=>$session_password]];
        $verify = lire_config('simplasso/verifier_certificat','on');

        $args['verify'] = ($verify==='on');
	    $response = $client->request('POST', $url_logiciel.'api/login_check',$args);

	    $reponse = json_decode($response->getBody(),true);
	    if (isset($reponse['token'])){
	        $token =$reponse['token'];
	        $client  = new GuzzleHttp\Client();
	        $headers = [
	            'Authorization' => 'Bearer ' . $token,
	            'Accept'        => 'application/json',
	        ];
            $args = [ 'headers' => $headers];
            $verify = lire_config('simplasso/verifier_certificat','on');
            $args['verify'] = ($verify==='on');
            $url_logiciel = lire_config('simplasso/url_logiciel');
	        $response = $client->request('GET', $url_logiciel.'api/me',$args);
	        $individu = json_decode($response->getBody(),true);
	        $ok=true;
	    }

	} catch (\GuzzleHttp\Exception\RequestException $ex) {
	    $erreurs[] = 'Probleme dans la connexion'.
        $ok=false;
    } catch (\GuzzleHttp\Exception\GuzzleException $ex) {
        $erreurs[] = 'Probleme dans la connexion'.$ex->getMessage();
        $ok=false;
    }


    if ($ok) {
        include_spip('inc/session');
        session_set('simplasso_user_token', $token);
        session_set('simplasso_statut', 'adherent');
           
        $id_individu = $individu['idIndividu'];
        $auteur = sql_fetsel('*', 'spip_auteurs', 'simplasso=' . $id_individu);
        if (empty($auteur)) {
            include_spip('action/editer_auteur');
            include_spip('inc/autoriser');
            if (($id_auteur = auteur_inserer()) > 0) {}
            $err = "";
            if ($id_auteur > 0) {
                $individu['simplasso']=$individu['idIndividu'];
                $err = auteur_modifier($id_auteur,$individu,true);

                autoriser_exception('modifier', 'auteur', $id_auteur);
                $tab=['statut'=>'6forum'];
                $err = auteur_modifier($id_auteur,$tab);

                unset($_COOKIE['spip_session']); // forcer la maj de la session
                autoriser_exception('modifier', 'auteur', $id_auteur, false);
                $auteur = sql_fetsel('*', 'spip_auteurs', 'id_auteur=' . $id_auteur);
                if( test_plugin_actif('accesrestreint')){
                    autoriser_exception('affecterzones', 'auteur', $id_auteur, true);
                    include_spip('inc/config');
                    $id_zone = lire_config('simplasso/id_zone_es');
                    if ($id_zone > 0){
                        include_spip('action/editer_zone');
                        zone_lier($id_zone,'auteur',$id_auteur);
                    }
                    autoriser_exception('affecterzones', 'auteur', $id_auteur, false);
                }
            }



        }
        else{
            unset($_COOKIE['spip_session']); // forcer la maj de la session
        }
        if($auteur){
            include_spip('inc/auth');
            auth_loger($auteur);
        }
       
    }


    return $erreurs;
}

/**
 * 

 * Teste l'autorisation d'accéder à l'espace privé une fois une connexion
 * réussie, si la cible est une URL privée.
 *
 * Dans le cas contraire, un message d'erreur est retourné avec un lien
 * pour se déconnecter.
 *
 * @return array
 *     - Erreur si un connecté n'a pas le droit d'acceder à l'espace privé
 *     - tableau vide sinon.
 **/
function login_simplasso_autoriser()
{
    include_spip('inc/autoriser');
    if (!autoriser('ecrire')) {
        $h = generer_url_action('logout', 'logout=public&url=' . urlencode(self()));

        return array(
            'message_erreur' => "<h1>"
                . _T('avis_erreur_visiteur')
                . "</h1><p>"
                . _T('texte_erreur_visiteur')
                . "</p><p class='retour'>[<a href='$h'>"
                . _T('icone_deconnecter') . "</a>]</p>"
        );
    }

    return array();
}

/**
 * Traitements du formulaire de login
 *
 * On arrive ici une fois connecté.
 * On redirige simplement sur l'URL cible désignée.
 *
 * @param string $cible
 *     URL de destination après identification.
 *     Cas spécifique : la valeur `@page_auteur` permet d'être redirigé
 *     après connexion sur le squelette public de l'auteur qui se connecte.
 * @param string $login
 *     Login de la personne à identifier (si connu)
 * @param null|bool $prive
 *     Identifier pour l'espace privé (true), public (false)
 *     ou automatiquement (null) en fonction de la destination de l'URL cible.
 * @return array
 *     Retours du traitement
 **/
function formulaires_login_simplasso_traiter_dist($cible = "", $login = "", $prive = null)
{
    $res = array();

    if(empty($cible)){
        $cible = generer_url_public('espace_adherent');
    }

    // Si on est connecte, envoyer vers la destination
    if ($cible and ($cible != self('&')) and ($cible != self())) {
        if (!headers_sent() and !isset($_GET['var_mode'])) {
            include_spip('inc/headers');
            $res['redirect'] = $cible;
        } else {
            $res['message_ok'] = inserer_attribut(
                "<a>" . _T('login_simplasso_par_ici') . "</a>",
                'href', $cible
            );
        }
    }

    return $res;
}
