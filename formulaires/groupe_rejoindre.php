<?php

/***************************************************************************\
 *  SPIP, Systeme de publication pour l'internet                           *
 *                                                                         *
 *  Copyright (c) 2001-2011                                                *
 *  Arnaud Martin, Antoine Pitrou, Philippe Riviere, Emmanuel Saint-James  *
 *                                                                         *
 *  Ce programme est un logiciel libre distribue sous licence GNU/GPL.     *
 *  Pour plus de details voir le fichier COPYING.txt ou l'aide en ligne.   *
 * \***************************************************************************/

if (!defined('_ECRIRE_INC_VERSION')) {
    return;
}

include_spip('inc/actions');
include_spip('inc/editer');


// http://doc.spip.org/@inc_editer_mot_dist
function formulaires_groupe_rejoindre_charger_dist($groupe) {
    $valeurs=[
        'groupe_nomcourt'=>$groupe
    ];
    $valeurs['_mes_saisies'] = simplasso_groupe_rejoindre();
    return $valeurs;
}



function formulaires_groupe_rejoindre_verifier_dist($groupe): array
{
    $mes_saisies = simplasso_groupe_rejoindre();
    $erreurs = saisies_verifier($mes_saisies);
    return $erreurs;
}


function formulaires_groupe_rejoindre_traiter_dist($groupe): array
{
    include_spip('inc/jsonrpc');
    $groupe_nomcourt = _request('groupe_nomcourt');
    $args=['groupe_nomcourt'=>$groupe_nomcourt];
    $reponse = interrogeAPI('groupe_rejoindre', $args);

    if (isset($reponse['ok']) && $reponse['ok']) {
        $url_redirect = _request('url_redirect',generer_url_public('espace_adherent'));
        $tab=[
            'redirect' => $url_redirect,
            'message_ok' => 'Votre participation a été ajouté.'
        ];
        return $tab;

    } else {
        return array('message_erreur' => 'Erreur');
    }
}


function simplasso_groupe_rejoindre(): array
{

    include_spip('inc/config');


    $tab_champs = [

        // Champ adresse
        'groupe_nomcourt' => array(
            'saisie' => 'hidden',
            'options' => array(
                'nom' => 'groupe_nomcourt',
                'obligatoire' => 'oui',
            )
        )
    ];

    return $tab_champs;
}
