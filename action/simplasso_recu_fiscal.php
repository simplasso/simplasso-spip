<?php



/**
 * Plugin simplasso
 **
 * Action simplasso _serveur
 */

if (!defined("_ECRIRE_INC_VERSION")) return;



function action_simplasso_recu_fiscal_dist($arg = null)
{
    include_spip('simplasso_fonctions');
    if (is_null($arg)) {
        $securiser_action = charger_fonction('securiser_action', 'inc');
        $arg = $securiser_action();
    }

    transfertRecuFiscal($arg);
    exit();

}

