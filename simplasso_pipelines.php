<?php


if (!defined("_ECRIRE_INC_VERSION")) return;

/**
 * ajouter un champ openID sur le formulaire CVT editer_auteur
 *
 * @param array $flux
 * @return array
 */
function simplasso_editer_contenu_objet($flux){
	if ($flux['args']['type']==='auteur') {
		$simplasso = recuperer_fond('formulaires/inc-simplasso', $flux['args']['contexte']);
		$flux['data'] = preg_replace('%(<(div|li) class=["\'][^"\']*editer_email(.*?)</\\2>)%is', '$1'."\n".$simplasso, $flux['data']);
	}
	return $flux;
}

/**
 * Ajouter la valeur id_individu de simplasso dans la liste des champs de la fiche auteur
 *
 * @param array $flux
 */
function simplasso_formulaire_charger($flux){
	// si le charger a renvoye false ou une chaine, ne rien faire
	if (is_array($flux['data'])){
		if ($flux['args']['form']==='editer_auteur'){
			$flux['data']['simplasso'] = ''; // un champ de saisie simplasso !
			if ($id_auteur = (int)($flux['data']['id_auteur']))
				$flux['data']['simplasso'] = sql_getfetsel('simplasso','spip_auteurs','id_auteur='.intval($id_auteur));
		}
		if ($flux['args']['form']==='inscription'){
			$flux['data']['_forcer_request'] = true; // forcer la prise en compte du post
			$flux['data']['url_simplasso'] = ''; // un champ de saisie simplasso !
			$flux['data']['simplasso'] = ''; // une url simplasso a se passer en hidden
			if ($erreur = _request('var_erreur'))
				$flux['data']['message_erreur'] = _request('var_erreur');
			elseif(_request('simplasso') AND (!_request('nom_inscription') OR !_request('mail_inscription')))
				$flux['data']['message_erreur'] = _T('simplasso:erreur_simplasso_info_manquantes');
		}
	}
	return $flux;
}


/**

 *
 * @param array $flux
 */
function simplasso_formulaire_verifier($flux){
	if ($flux['args']['form']==='editer_auteur'){
		if ($simplasso = _request('simplasso')){
			include_spip('inc/simplasso');
			if (!verifier_simplasso($simplasso))
				$flux['data']['simplasso']=_T('simplasso:erreur_simplasso');
		}
	}

	return $flux;
}



/**

 * 
 * @param array $flux
 * @return array
 */
function simplasso_pre_edition($flux){
	if ($flux['args']['table']==='spip_auteurs') {
        $simplasso = _request('simplasso');
		if (!is_null($simplasso)) {
			include_spip('inc/simplasso');
			$flux['data']['simplasso'] = $simplasso;
		}
	}
	return $flux;
}

/**
 * @param array $flux 
 */
function simplasso_afficher_contenu_objet($flux){
	if ($flux['args']['type']==='auteur'
		AND $id_auteur = $flux['args']['id_objet']
		AND $simplasso = sql_getfetsel('simplasso','spip_auteurs','id_auteur='.intval($id_auteur))
	){
		$flux['data'] .= propre("<div class='champ contenu_simplasso'><img src='".find_in_path('images/simplasso-16.png')
			."' alt='"._T('simplasso:simplasso')."' width='16' height='16' />"
			. " [->$simplasso]</div>");

	}

	return $flux;
}


function simplasso_insert_head($texte){
    $texte .= '<link rel="stylesheet" type="text/css" href="'.find_in_path('css/simplasso_spip.css').'" media="all" />'."\n";
    $texte .= '<script type="text/javascript" src="'.find_in_path('js/simplasso.js').'"></script>';
    return $texte;
}
