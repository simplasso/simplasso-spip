function initialiser_formulaire_don(){

    $('.editer_montant_don div.choix:last').append($('#champ_montant_don_autre'));
    $('#champ_montant_don_autre').attr('placeholder',$('.editer_montant_don div.choix:last label').text());
    $('.editer_montant_don div.choix:last label').hide();
    $('#champ_montant_don_autre').width('100px');

    $('.editer_montant_don_autre').remove();
    $('#champ_montant_don_autre').click(function(){
        $(".editer_montant_don input[name=montant_don][value=-1]").prop('checked', true);
    });
    $('.editer_montant_don div.choix').click(function(){
        $('.editer_montant_don div.choix').removeClass('on');
        $(this).addClass('on');
    });

}